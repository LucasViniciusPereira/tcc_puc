﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using PUC.TCC.Api.Class;
using System.Collections.Generic;
using System.Net;

namespace PUC.TCC.Api.Filters
{
    public class HandlerExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            var erros = new List<string> { context.Exception.Message };
            context.Result = new ObjectResult(new ErrorDtoOutput("Ocorreu erro no servidor, tente novamente", erros)) { StatusCode = (int)HttpStatusCode.InternalServerError };
        }
    }
}
