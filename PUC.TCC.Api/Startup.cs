﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PUC.TCC.Api.Extensions;
using PUC.TCC.Api.Filters;
using PUC.TCC.Api.IoC;
using PUC.TCC.Domain.Models;
using PUC.TCC.Infra.EntityFramework;
using System;

namespace PUC.TCC.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; set; }
        public IHostingEnvironment Environment { get; set; }

        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<Context>(c => c.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
              options => { options.CommandTimeout((int)TimeSpan.FromMinutes(5).TotalSeconds); }).UseLazyLoadingProxies());

            services.ConfigureIdentity(Configuration);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.ConfigureSwagger(Environment.EnvironmentName);
            services.RegisterServices(Configuration);
            services.AddCors();

            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(HandlerExceptionFilterAttribute));
            });
        }

        public void Configure(IApplicationBuilder app,
            IHostingEnvironment env,
            ApplicationDbContext context,
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = config.Build();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            new IdentityInitializer(context, userManager, roleManager).Initialize();

            app.UseCors(builder => builder
                  .AllowAnyOrigin()
                  .AllowAnyMethod()
                  .AllowAnyHeader()
                  .AllowCredentials()
                  .WithMethods("GET", "POST", "DELETE", "PUT", "OPTIONS"));

            app.UseHttpsRedirection();
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("../swagger/v1/swagger.json", "PUC Minas-TCC - v1");
                c.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.None);
            });
        }
    }
}
