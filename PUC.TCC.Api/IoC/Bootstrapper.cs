﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PUC.TCC.Api.Class;
using PUC.TCC.Api.RabbitMQ;
using PUC.TCC.Api.RabbitMQ.Queues;
using PUC.TCC.Application.Services.Autenticacao;
using PUC.TCC.Application.Services.Barragem;
using PUC.TCC.Application.Services.IntegracaoS2id;
using PUC.TCC.Application.Services.Notificacao;
using PUC.TCC.Application.Services.RiscoRompimento;
using PUC.TCC.Domain.Interfaces;
using PUC.TCC.Infra.EntityFramework;
using PUC.TCC.Infra.Repository;

namespace PUC.TCC.Api.IoC
{
    public static class Bootstrapper
    {
        public static void RegisterServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IBarragemService, BarragemService>();
            services.AddTransient<INotificacaoService, NotificacaoService>();

            services.AddTransient<IBarragemRepository, BarragemRepository>();
            services.AddTransient<INotificacaoRepository, NotificacaoRepository>();

            services.AddTransient<IAutenticacaoService, AutenticacaoService>();

            services.AddTransient<IRiscoRompimentoRepository, RiscoRompimentoRepository>();
            services.AddTransient<IRiscoRompimentoService, RiscoRompimentoService>();

            services.AddTransient<IIntegracaoS2idService, IntegracaoS2idService>();
            services.AddTransient<IIntegracaoS2idRepository, IntegracaoS2idRepository>();

            services.AddScoped<RabbitMQFactory>()
                .Configure<RabbitMQConfiguration>(options => configuration.Bind("RabbitMQConfigutations", options));
            services.AddTransient<IntegracaoModuloSegurancaComunicacao>();

            services.AddTransient<Presenter>();

            services.AddScoped<Context>();
            services.AddScoped(p => new Context(p.GetService<DbContextOptions<Context>>()));

            services.AddScoped<ApplicationDbContext>();
            services.AddScoped(p => new ApplicationDbContext(p.GetService<DbContextOptions<ApplicationDbContext>>()));
        }
    }
}
