﻿using Microsoft.AspNetCore.Mvc;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PUC.TCC.Api.Class
{
    public class Presenter
    {
        public IActionResult ViewModel { get; protected set; }

        public void Output(dynamic model)
        {
            var isModel = model == null;
            var isBoolean = model is bool && model == false;
            var isEnumerable = model is IEnumerable && !((IEnumerable<object>)model).Any();

            if (isModel || isBoolean || isEnumerable)
            {
                ViewModel = new NoContentResult();
            }
            else
            {
                ViewModel = new ObjectResult(model);
            }
        }
    }
}
