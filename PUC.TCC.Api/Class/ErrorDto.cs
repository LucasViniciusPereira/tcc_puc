﻿using System.Collections.Generic;

namespace PUC.TCC.Api.Class
{
    public class ErrorDtoOutput
    {
        public List<string> Errors { get; private set; }
        public string Message { get; private set; }

        public ErrorDtoOutput(string message, List<string> errors)
        {
            Message = message;
            Errors = errors;
        }
    }
}
