﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PUC.TCC.Api.Class;
using PUC.TCC.Application.Services.Barragem;
using PUC.TCC.Application.Services.Barragem.Dtos;
using System.ComponentModel.DataAnnotations;

namespace PUC.TCC.Api.Controllers
{
    [Authorize("Bearer")]
    [Route("api/[controller]")]
    public class BarragemController : ControllerBase
    {
        private readonly IBarragemService _barragemService;
        private readonly Presenter _presenter;

        public BarragemController(Presenter presenter, IBarragemService barragemService)
        {
            _presenter = presenter;
            _barragemService = barragemService;
        }

        /// <summary>
        /// Método para buscar todas as barragens
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var result = _barragemService.BuscarBarragem();
            _presenter.Output(result);
            return _presenter.ViewModel;
        }

        /// <summary>
        /// Método para buscar uma barragem especifica pelo Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "GetById")]
        public IActionResult Get(int id)
        {
            var result = _barragemService.BuscarBarragem(id);
            _presenter.Output(result);
            return _presenter.ViewModel;
        }

        /// <summary>
        /// Método para realizar uma pelo nome da barragem
        /// </summary>
        /// <param name="nome"></param>
        /// <returns></returns>
        [HttpGet("Pesquisar")]
        public IActionResult Search([RegularExpression(@"^.{3,}$", ErrorMessage = "Mínimo de 3 caracteres")]string nome)
        {
            var result = _barragemService.PesquisarBarragem(nome);
            _presenter.Output(result);
            return _presenter.ViewModel;
        }

        /// <summary>
        /// Método para cadastrar uma barragem no sistema
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("criarBarragem")]
        public IActionResult Post([FromBody]BarragemDtoInput model)
        {
            var result = _barragemService.CriarBarragem(model);
            _presenter.Output(result);
            return _presenter.ViewModel;
        }

        /// <summary>
        /// Método para vincular o medidor(Sensor) na barragem
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("vincularMedidor")]
        public IActionResult Medidor([FromBody]MedidorDtoInput model)
        {
            var result = _barragemService.VincularMedidor(model);
            _presenter.Output(result);
            return _presenter.ViewModel;
        }

        /// <summary>
        /// Método para desativar a barregem movendo para o status de desativada
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("desativarBarragem")]
        public IActionResult DesativarBarragem(int id)
        {
            var result = _barragemService.DesativarBarragem(id);
            _presenter.Output(result);
            return _presenter.ViewModel;
        }

        /// <summary>
        /// Método para alterar a capacidade da barragem
        /// </summary>
        /// <param name="id"></param>
        /// <param name="novaCapacidade"></param>
        /// <returns></returns>
        [HttpPost("alterarCapacidadeBarragem")]
        public IActionResult AlterarCapacidadeBarragem(int id, decimal novaCapacidade)
        {
            var result = _barragemService.AlterarCapacidadeBarragem(id, novaCapacidade);
            _presenter.Output(result);
            return _presenter.ViewModel;
        }
    }
}