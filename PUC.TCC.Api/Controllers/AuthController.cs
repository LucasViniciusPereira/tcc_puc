﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PUC.TCC.Api.RabbitMQ.Queues;
using PUC.TCC.Application.Services.Autenticacao;
using PUC.TCC.Application.Services.Autenticacao.Dtos;
using System.Threading.Tasks;

namespace PUC.TCC.Api.Controllers
{
    [ApiController]
    [AllowAnonymous]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IAutenticacaoService _autenticacaoService;
        private readonly IntegracaoModuloSegurancaComunicacao _integracaoModuloSegurancaComunicacao;

        public AuthController(IAutenticacaoService autenticacaoService, IntegracaoModuloSegurancaComunicacao integracaoModuloSegurancaComunicacao)
        {
            _autenticacaoService = autenticacaoService;
            _integracaoModuloSegurancaComunicacao = integracaoModuloSegurancaComunicacao;
        }
        
        /// <summary>
        /// Método para autenticar no sistema
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("Acessar")]
        public IActionResult Login([FromBody]UsuarioDto model)
        {
            var token = _autenticacaoService.Login(model);
            return Ok(token);
        }

        /// <summary>
        /// Método para cadastrar um novo usuário no sistema
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("Cadastar")]
        public IActionResult Post([FromBody]UsuarioCreateDto model)
        {
            _autenticacaoService.Cadastar(model);
            return Ok();
        }

        /// <summary>
        /// Método para sair do sistema
        /// </summary>
        /// <returns></returns>
        [HttpPost("Deslogar")]
        public async Task<IActionResult> Logout()
        {
            await _autenticacaoService.Logout();
            return Ok();
        }
    }
}