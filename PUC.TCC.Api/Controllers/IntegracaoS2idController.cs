﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PUC.TCC.Api.Class;
using PUC.TCC.Application.Services.IntegracaoS2id;

namespace PUC.TCC.Api.Controllers
{
    [Authorize("Bearer")]
    [ApiController]
    [Route("api/[controller]")]
    public class IntegracaoS2idController : ControllerBase
    {
        private readonly IIntegracaoS2idService _service;
        private readonly Presenter _presenter;

        public IntegracaoS2idController(Presenter presenter, IIntegracaoS2idService service)
        {
            _presenter = presenter;
            _service = service;
        }

        /// <summary>
        /// Método para buscar todos os dados de integração com o sistema S2id da Defesa Civil
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var result = _service.BuscarTodos();
            _presenter.Output(result);
            return _presenter.ViewModel;
        }
    }
}