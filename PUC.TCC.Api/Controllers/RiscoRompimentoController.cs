﻿using Microsoft.AspNetCore.Mvc;
using PUC.TCC.Api.RabbitMQ.Queues;
using PUC.TCC.Application.Services.Barragem;
using PUC.TCC.Application.Services.RiscoRompimento;

namespace PUC.TCC.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RiscoRompimentoController : ControllerBase
    {
        private readonly IRiscoRompimentoService _riscoRompimentoService;
        private readonly IBarragemService _barragemService;
        private readonly IntegracaoModuloSegurancaComunicacao _integracaoModuloSegurancaComunicacao;

        public RiscoRompimentoController(
            IRiscoRompimentoService riscoRompimentoService,
            IBarragemService barragemService,
            IntegracaoModuloSegurancaComunicacao integracaoModuloSegurancaComunicacao
            )
        {
            _riscoRompimentoService = riscoRompimentoService;
            _barragemService = barragemService;
            _integracaoModuloSegurancaComunicacao = integracaoModuloSegurancaComunicacao;
        }

        /// <summary>
        /// Método responsável por emitir o risco de rompimento da barragem
        /// </summary>
        /// <param name="barragemId"></param>
        /// <returns></returns>
        [HttpPost("emitirRiscoRompimento")]
        public IActionResult Emitir(int barragemId)
        {
            var result = _riscoRompimentoService.EmitirRiscoDeRompimento(barragemId);
            _integracaoModuloSegurancaComunicacao.SendMessage(result);
            return Ok(result.Id);
        }

        /// <summary>
        /// Método responsável por cancelar o risco de rompimento da barragem
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("cancelarRiscoRompimento")]
        public IActionResult Cancelar(int id)
        {
            var barragemId = _riscoRompimentoService.CancelarRiscoDeRompimento(id);
            var result = _barragemService.BuscarBarragem(barragemId);
            return Ok(result);
        }

        /// <summary>
        /// Método responsável confirmar o risco de rompimento e mover ela para o status de alerta máximo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("confirmarRompimento")]
        public IActionResult Confirmar(int id)
        {
            var barragemId = _riscoRompimentoService.ConfirmarRompimento(id);
            var result = _barragemService.BuscarBarragem(barragemId);
            return Ok(result);
        }

        /// <summary>
        /// Método responsável por buscar os risco de rompimento através do identificador da barragem
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}", Name = "buscarRiscoRompimento")]
        public IActionResult Buscar(int id)
        {
            var result = _riscoRompimentoService.BuscarRiscoRompimento(id);
            return Ok(result);
        }
    }
}