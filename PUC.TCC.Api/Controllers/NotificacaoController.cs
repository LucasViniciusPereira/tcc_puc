﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PUC.TCC.Api.Class;
using PUC.TCC.Application.Services.Notificacao;
using PUC.TCC.Application.Services.Notificacao.Dtos;

namespace PUC.TCC.Api.Controllers
{
    [Route("api/[controller]")]
    public class NotificacaoController : ControllerBase
    {
        private readonly INotificacaoService _notificacaoService;
        private readonly Presenter _presenter;

        public NotificacaoController(Presenter presenter, INotificacaoService notificacaoService)
        {
            _presenter = presenter;
            _notificacaoService = notificacaoService;
        }

        /// <summary>
        /// Método utilizado para notificar a barragem, sendo utilizado pelo medidor(sensor)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(Name = "NotificarBarragem")]
        public IActionResult Post([FromBody]NotificacaoDtoInput model)
        {
            var result = _notificacaoService.NotificarBarragem(model);
            _presenter.Output(result);
            return _presenter.ViewModel;
        }
    }
}