﻿using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;
using System.IO;

namespace PUC.TCC.Api.Extensions
{
    public static class SwaggerExtension
    {
        public static void ConfigureSwagger(this IServiceCollection service, string enviromentName)
        {
            service.AddSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.IncludeXmlComments(Path.ChangeExtension(typeof(Startup).Assembly.Location, "xml"));
                options.CustomSchemaIds(x => x.FullName);

                options.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                var security = new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[] { }},
                };
                options.AddSecurityRequirement(security);
                options.SwaggerDoc("v1", new Info
                {
                    Title = $"PUC Minas-TCC",
                    Version = "v1",
                    Description = $"Projeto de conclusão do curso de Arquitetura de Software Distribuido - Sistema de Monitoramento de Barragens - Ambiente: {enviromentName}",
                });
            });
        }
    }
}
