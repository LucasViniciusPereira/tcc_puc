﻿namespace PUC.TCC.Api.RabbitMQ
{
    public class RabbitMQConfiguration
    {
        public string HostName { get; set; }
        public string Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Uri { get; set; }
    }
}
