﻿using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using System;

namespace PUC.TCC.Api.RabbitMQ
{
    public class RabbitMQFactory : IDisposable
    {
        public readonly IConnection connection;
        public readonly IModel channel;
        public RabbitMQFactory(IOptions<RabbitMQConfiguration> options)
        {
            var factory = new ConnectionFactory()
            {
                Uri = new Uri(options.Value.Uri)
                //HostName = options.Value.HostName,
                //UserName = options.Value.UserName,
                //Password = options.Value.Password,
                //Port = Convert.ToInt32(options.Value.Port)
            };
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
        }
        public void Dispose()
        {
            channel.Close();
            connection.Close();
        }
    }
}
