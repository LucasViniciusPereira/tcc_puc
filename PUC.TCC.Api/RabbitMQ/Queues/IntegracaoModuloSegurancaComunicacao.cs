﻿using PUC.TCC.Application.Services.RiscoRompimento.Dtos;
using RabbitMQ.Client;
using System.Text;

namespace PUC.TCC.Api.RabbitMQ.Queues
{
    public class IntegracaoModuloSegurancaComunicacao
    {
        private readonly RabbitMQFactory _rabbitMQ;
        public IntegracaoModuloSegurancaComunicacao(RabbitMQFactory rabbitMQ)
        {
            _rabbitMQ = rabbitMQ;
        }

        public void SendMessage(RiscoRompimentoDtoOutput model)
        {
            var message = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            var data = Encoding.ASCII.GetBytes(message);

            var queueName = "tcc_modulo_seguranca";
            var exchange = "tcc";

            _rabbitMQ.channel.QueueDeclare(queueName, durable: false, exclusive: false, autoDelete: true, null);
            _rabbitMQ.channel.ExchangeDeclare(exchange, "fanout");
            _rabbitMQ.channel.BasicPublish(exchange, queueName, null, data);
        }
    }
}
