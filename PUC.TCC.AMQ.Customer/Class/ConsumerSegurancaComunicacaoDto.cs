﻿namespace PUC.TCC.AMQ.Consumers.Class
{
    public class ConsumerSegurancaComunicacaoDto
    {
        public int Id { get; set; }
        public string Barragem { get; set; }
    }
}
