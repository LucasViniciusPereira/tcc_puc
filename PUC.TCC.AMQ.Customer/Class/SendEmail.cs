﻿using MailKit.Net.Smtp;
using MimeKit;
using System;

namespace PUC.TCC.AMQ.Consumers.Class
{
    public class EmailService
    {
        public void Send(ConsumerSegurancaComunicacaoDto model)
        {
            MimeMessage message = new MimeMessage();

            MailboxAddress from = new MailboxAddress("PUC TCC - Monitoramento de barragens", "puctcc1143455@gmail.com");
            message.From.Add(from);
            MailboxAddress to = new MailboxAddress("Usuário", "lv.lucasvin@gmail.com");
            message.To.Add(to);
            message.Subject = $"Integração Modulo - Segurança e Comunicação";

            var bodyBuilder = new BodyBuilder();
            var body = $@"<h4>Comunicado URGENTE - { model.Barragem.ToUpper()}</h4>
                    <p>protocolo para início do Plano de Ação de Emergência de Barragens de Mineração (PAEBM) 
                    para a <b>{ model.Barragem.ToUpper()}</b>, nesta { DateTime.Now.ToShortDateString()} </p>
                    <p>Essa medida preventiva se faz necessária tendo em vista o fato do auditor independente ter informado a 
                    condição crítica de estabilidade da barragem. Foi acionada a sirene de alerta que cobre a Zona de Autossalvamento (ZAS), 
                    como reforço de medida preventiva.</p><p>Atenciosamente</p>";

            bodyBuilder.HtmlBody = body;
            message.Body = bodyBuilder.ToMessageBody();

            SmtpClient client = new SmtpClient();
            client.Connect("smtp.gmail.com", 587, false);
            client.Authenticate("puctcc1143455@gmail.com", "Admin@123");

            client.Send(message);
            client.Disconnect(true);
            client.Dispose();
        }
    }
}
