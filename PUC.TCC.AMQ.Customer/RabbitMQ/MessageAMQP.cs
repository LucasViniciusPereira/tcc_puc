﻿using PUC.TCC.AMQ.Customer.Consumers;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace PUC.TCC.AMQ.Customer
{
    public class MessageAMQP
    {
        public void Listenner(string queueName, string exchange = default)
        {
            var hasExchange = !string.IsNullOrEmpty(exchange);

            using (var cnn = new ConnectionAMQP().Get())
            using (var channel = cnn.CreateModel())
            {
                if (hasExchange)
                {
                    channel.ExchangeDeclare(exchange: exchange, type: "fanout");
                    queueName = channel.QueueDeclare().QueueName;
                    channel.QueueBind(queue: queueName,
                                      exchange: exchange,
                                      routingKey: "");
                }

                var consumer = new EventingBasicConsumer(channel);
                channel.BasicQos(0, 1, false);
                consumer.Received += new ConsumerSegurancaComunicacao().Consumer_Received;
                channel.BasicConsume(queueName, true, consumer);
                channel.ConfirmSelect();

                while (true)
                {
                }
            }
        }
    }
}
