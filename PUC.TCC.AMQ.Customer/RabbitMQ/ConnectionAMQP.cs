﻿using RabbitMQ.Client;
using System;

namespace PUC.TCC.AMQ.Customer
{
    public class ConnectionAMQP
    {
        private readonly ConnectionFactory _instance;
        private readonly IConnection _connection;
        public ConnectionAMQP()
        {
            if (_instance == null)
            {
                _instance = Create();
                _connection = _instance.CreateConnection();
            }

        }

        private ConnectionFactory Create()
        {
            return new ConnectionFactory { Uri = new Uri("amqps://gdnfhfep:ePLOlIvGy35MK9eUdbWUM0sVoCNQIb3Z@skunk.rmq.cloudamqp.com/gdnfhfep") };
        }

        public IConnection Get()
        {
            return _connection;
        }
    }
}
