﻿using Newtonsoft.Json;
using PUC.TCC.AMQ.Consumers.Class;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace PUC.TCC.AMQ.Customer.Consumers
{
    public class ConsumerSegurancaComunicacao : ConsumerBase
    {
        public override void Consumer_Received(object sender, BasicDeliverEventArgs e)
        {
            var message = Encoding.UTF8.GetString(e.Body);
            var data = DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToShortTimeString();
            Console.WriteLine(Environment.NewLine + "[Mensagem]: " + data + " " + message);
            var model = JsonConvert.DeserializeObject<ConsumerSegurancaComunicacaoDto>(message);
            new EmailService().Send(model);
        }
    }
}
