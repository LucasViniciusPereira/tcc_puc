﻿using RabbitMQ.Client.Events;

namespace PUC.TCC.AMQ.Customer.Consumers
{
    public abstract class ConsumerBase
    {
        public abstract void Consumer_Received(object sender, BasicDeliverEventArgs e);
    }
}
