﻿using System;

namespace PUC.TCC.AMQ.Customer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Serviço de mensageria inicializado - AMQP");

            var queue = "tcc_modulo_seguranca";
            var exchange = "tcc";
            new MessageAMQP().Listenner(queue, exchange);
        }
    }
}
