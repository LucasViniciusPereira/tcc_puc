﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace PUC.TCC.Hangfire.IoC
{
    public class ServiceLocatorApp
    {
        private readonly ServiceProvider _currentServiceProvider;
        private static ServiceProvider _serviceProvider;

        public ServiceLocatorApp(ServiceProvider currentServiceProvider)
        {
            _currentServiceProvider = currentServiceProvider;
        }

        public static ServiceLocatorApp Current
        {
            get
            {
                return new ServiceLocatorApp(_serviceProvider);
            }
        }

        public static void SetLocatorProvider(ServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public object GetInstance(Type serviceType)
        {
            return _currentServiceProvider.GetService(serviceType);
        }

        public static TService GetInstance<TService>()
        {
            return _serviceProvider.GetService<TService>();
        }
    }
}
