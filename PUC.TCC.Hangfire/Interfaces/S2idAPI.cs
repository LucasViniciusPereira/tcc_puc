﻿using PUC.TCC.Application.Services.IntegracaoS2id.Dto;
using Refit;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PUC.TCC.Hangfire.Interfaces
{
    public interface S2idAPI
    {
        [Get("/5d4efc6e30000091632960b2")]
        Task<IEnumerable<IntegracaoS2idDto>> GetDesastres();
    }
}
