﻿using Newtonsoft.Json.Converters;
using System;
using System.Reflection;

namespace PUC.TCC.Hangfire.Class
{
    public class CustomUrlParameterFormatter : Refit.DefaultUrlParameterFormatter
    {
        public override string Format(object parameterValue, ParameterInfo parameterInfo)
        {
            string result = string.Empty;
            if (typeof(DateTime?).IsAssignableFrom(parameterInfo.ParameterType) && parameterValue != null)
                result = ((DateTime)parameterValue).ToString("dd-MM-yyyy");

            return result;
        }
    }
}
