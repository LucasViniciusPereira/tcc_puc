﻿using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PUC.TCC.Application.Services.IntegracaoS2id;
using PUC.TCC.Domain.Interfaces;
using PUC.TCC.Hangfire.IoC;
using PUC.TCC.Hangfire.Jobs;
using PUC.TCC.Infra.EntityFramework;
using PUC.TCC.Infra.Repository;
using System;
using System.Globalization;
using System.Threading;

namespace PUC.TCC.Hangfire
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddDbContext<Context>(c => c.UseSqlServer(Configuration.GetConnectionString("HangfireConnection"),
              options => { options.CommandTimeout((int)TimeSpan.FromMinutes(5).TotalSeconds); }));

            services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage(Configuration.GetConnectionString("HangfireConnection"), new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true
                }));

            services.AddHangfireServer();

            services.AddTransient<IIntegracaoS2idService, IntegracaoS2idService>();
            services.AddTransient<IIntegracaoS2idRepository, IntegracaoS2idRepository>();

            services.AddScoped<Context>();
            services.AddScoped(p => new Context(p.GetService<DbContextOptions<Context>>()));

            //ServiceLocator
            ServiceLocatorApp.SetLocatorProvider(services.BuildServiceProvider());
        }

        public void Configure(IApplicationBuilder app, IBackgroundJobClient backgroundJobs, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();

            //UTC - Brazillian
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("pt-BR");
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.CurrentCulture;
            var BrTimeZone = TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time");

            app.UseHangfireDashboard();
            BackgroundJob.Enqueue<JobIntegracaoS2id>(c => c.Execute());
            //RecurringJob.AddOrUpdate<JobIntegracaoS2id>("JobIntegracaoS2id", c => c.Execute(), "0 * * * *", BrTimeZone);

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
