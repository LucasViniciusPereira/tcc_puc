﻿using PUC.TCC.Application.Services.IntegracaoS2id;
using PUC.TCC.Domain.Interfaces;
using PUC.TCC.Hangfire.Class;
using PUC.TCC.Hangfire.Interfaces;
using PUC.TCC.Hangfire.IoC;
using Refit;
using System;

namespace PUC.TCC.Hangfire.Jobs
{
    public class JobIntegracaoS2id
    {
        private readonly string URL;
        private readonly IIntegracaoS2idService _service;
        private readonly IIntegracaoS2idRepository _repo;
        public JobIntegracaoS2id()
        {
            URL = "http://www.mocky.io/v2";
            _service = ServiceLocatorApp.GetInstance<IIntegracaoS2idService>();
            _repo = ServiceLocatorApp.GetInstance<IIntegracaoS2idRepository>();
        }

        public void Execute()
        {
            var services = RestService.For<S2idAPI>(URL);

            var result = services.GetDesastres().Result;

            foreach (var item in result)
            {
                var itemIntegracao = _service.Buscar(c => c.Cobrade == item.Cobrade);

                if (itemIntegracao != null)
                    continue;

                _service.Salvar(new Domain.Models.IntegracaoS2id
                {
                    Ativo = item.Ativo,
                    Cobrade = item.Cobrade,
                    DataDecreto = DateTime.Parse(item.DataDecreto),
                    Descricao = item.Descricao,
                    //ssId = item.Id,
                    Latitude = double.Parse(item.Latitude),
                    Longitude = double.Parse(item.Longitude),
                    Municipio = item.Municipio,
                    Processo = item.Processo,
                    Regiao = item.Regiao,
                    Rito = item.Rito,
                    SeEcp = item.SeEcp,
                    Tipo = item.Tipo,
                    Uf = item.Uf,
                    Site = "https://s2id.mi.gov.br"
                });
                _repo.SaveChanges();
            }
        }
    }
}
