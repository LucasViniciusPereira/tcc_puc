﻿using PUC.TCC.Application.Services.RiscoRompimento.Dtos;

namespace PUC.TCC.Application.Services.RiscoRompimento
{
    public interface IRiscoRompimentoService
    {
        RiscoRompimentoDtoOutput EmitirRiscoDeRompimento(int barragemId);
        int CancelarRiscoDeRompimento(int riscoRompimentoId);
        int ConfirmarRompimento(int riscoRompimentoId);
        RiscoRompimentoDtoInput BuscarRiscoRompimento(int riscoRompimentoId);
    }
}
