﻿using System;

namespace PUC.TCC.Application.Services.RiscoRompimento.Dtos
{
    public class RiscoRompimentoDtoInput
    {
        public int RiscoRompimentoId { get; set; }
        public bool Rompimento { get; set; }
        public bool Ativo { get; set; }
        public string Mensagem { get; set; }
        public DateTime? DataOcorrencia { get; set; }
    }

    public class RiscoRompimentoDtoOutput
    {
        public int Id { get; private set; }
        public string Barragem { get; private set; }

        public RiscoRompimentoDtoOutput(int id, string nome)
        {
            Id = id;
            Barragem = nome;
        }
    }
}
