﻿using PUC.TCC.Application.Services.RiscoRompimento.Dtos;
using PUC.TCC.Domain.Interfaces;
using System;
using System.Linq;
using Model = PUC.TCC.Domain.Models;

namespace PUC.TCC.Application.Services.RiscoRompimento
{
    public class RiscoRompimentoService : IRiscoRompimentoService
    {
        private readonly IBarragemRepository _repoBarragem;
        private readonly IRiscoRompimentoRepository _repoRiscoRompimento;

        public RiscoRompimentoService(IBarragemRepository repoBarragem, IRiscoRompimentoRepository repoRiscoRompimento)
        {
            _repoBarragem = repoBarragem;
            _repoRiscoRompimento = repoRiscoRompimento;
        }

        public RiscoRompimentoDtoOutput EmitirRiscoDeRompimento(int barragemId)
        {
            var barragem = _repoBarragem.Get(barragemId);

            if (barragem == null)
                throw new Exception($"Não foi possível localizar a barragem {barragemId}");

            var mensagem = $"Foi constatado que a <strong>{barragem.Nome}</strong> tem risco de rompimento, " +
                $"já foi lançado um comunicado as " +
                $"comunidades que vivem próximo do local.";

            var riscoRompimento = new Model.RiscoRompimento(barragem.BarragemId, mensagem);

            _repoRiscoRompimento.Add(riscoRompimento);
            _repoRiscoRompimento.SaveChanges();

            return new RiscoRompimentoDtoOutput(riscoRompimento.RiscoRompimentoId, riscoRompimento.Barragem.Nome);
        }

        public int CancelarRiscoDeRompimento(int riscoRompimentoId)
        {
            var riscoRompimento = _repoRiscoRompimento.Get().Where(c => c.RiscoRompimentoId == riscoRompimentoId && c.Ativo).SingleOrDefault();

            if (riscoRompimento == null)
                throw new Exception($"Não foi possível localizar o risco de rompimento {riscoRompimentoId}");

            riscoRompimento.CancelarRiscoRompimento();
            _repoRiscoRompimento.SaveChanges();

            return riscoRompimento.BarragemId;
        }

        public int ConfirmarRompimento(int riscoRompimentoId)
        {
            var riscoRompimento = _repoRiscoRompimento.Get().Where(c => c.RiscoRompimentoId == riscoRompimentoId && c.Ativo).SingleOrDefault();

            if (riscoRompimento == null)
                throw new Exception($"Não foi possível localizar o risco de rompimento {riscoRompimentoId}");

            riscoRompimento.ConfirmarRompimento();
            _repoRiscoRompimento.SaveChanges();

            return riscoRompimento.BarragemId;
        }

        public RiscoRompimentoDtoInput BuscarRiscoRompimento(int riscoRompimentoId)
        {
            var riscoRompimento = _repoRiscoRompimento.Get().Where(c => c.RiscoRompimentoId == riscoRompimentoId).SingleOrDefault();

            if (riscoRompimento == null)
                throw new Exception($"Não foi possível localizar o Risco de rompimento {riscoRompimentoId}");

            return new RiscoRompimentoDtoInput
            {
                RiscoRompimentoId = riscoRompimento.RiscoRompimentoId,
                Mensagem = riscoRompimento.Mensagem,
                Rompimento = riscoRompimento.Rompimento,
                DataOcorrencia = riscoRompimento.DataOcorrencia,
                Ativo = riscoRompimento.Ativo
            };
        }
    }
}
