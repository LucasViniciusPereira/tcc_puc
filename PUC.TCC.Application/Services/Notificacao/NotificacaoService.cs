﻿using PUC.TCC.Application.Services.Notificacao.Dtos;
using PUC.TCC.Domain.Interfaces;
using System;
using System.Linq;
using Model = PUC.TCC.Domain.Models;

namespace PUC.TCC.Application.Services.Notificacao
{
    public class NotificacaoService : INotificacaoService
    {
        private readonly INotificacaoRepository _repo;
        private readonly IBarragemRepository _repoBarragem;

        public NotificacaoService(INotificacaoRepository repo, IBarragemRepository repoBarragem)
        {
            _repo = repo;
            _repoBarragem = repoBarragem;
        }

        public bool NotificarBarragem(NotificacaoDtoInput model)
        {
            var medidor = _repoBarragem.GetMedidor(model.MedidorId);

            if (medidor == null)
                throw new Exception("Não foi possível localizar o medidor responsável pela notificação");

            var barragem = _repoBarragem.Get(medidor.BarragemId);

            if (barragem == null)
                throw new Exception($"O Medidor {medidor.Nome} não está vinculado a nenhuma barragem");

            var notificacao = new Model.Notificacao { Ativo = true, DataCricao = DateTime.Now, Descricao = model.Mensagem, ETipoCriticidade = model.ETipoCriticidade };

            _repo.Add(notificacao);
            _repo.AddBarragemNotificacao(new Model.BarragemNotificacao { Barragem = barragem, Medidor = medidor, Notificacao = notificacao });

            barragem.DefinirStatusBarragem();

            return _repo.SaveChanges();
        }

        public void Dispose()
        {
            _repo.Dispose();
            _repoBarragem.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
