﻿using PUC.TCC.Application.Services.Notificacao.Dtos;
using System;

namespace PUC.TCC.Application.Services.Notificacao
{
    public interface INotificacaoService : IDisposable
    {
        bool NotificarBarragem(NotificacaoDtoInput model);
    }
}
