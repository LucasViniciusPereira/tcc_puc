﻿using PUC.TCC.Domain.Enuns;
using System;
using System.Collections.Generic;
using System.Text;

namespace PUC.TCC.Application.Services.Notificacao.Dtos
{
    public class NotificacaoDtoOutput
    {
        public int NotificacaoId { get; set; }
        public int MedidorId { get; set; }
        public string Mensagem { get; set; }
        public ETipoCriticidade ETipoCriticidade { get; set; }
    }
    public class NotificacaoDtoInput
    {
        public int MedidorId { get; set; }
        public string Mensagem { get; set; }
        public ETipoCriticidade ETipoCriticidade { get; set; }
    }
}
