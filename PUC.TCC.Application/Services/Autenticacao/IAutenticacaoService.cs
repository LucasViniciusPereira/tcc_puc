﻿using PUC.TCC.Application.Services.Autenticacao.Dtos;
using PUC.TCC.Domain.Models;
using System.Threading.Tasks;

namespace PUC.TCC.Application.Services.Autenticacao
{
    public interface IAutenticacaoService
    {
        TokenDto Login(UsuarioDto usuario);
        void Cadastar(UsuarioCreateDto usuario);
        Task Logout();
    }
}
