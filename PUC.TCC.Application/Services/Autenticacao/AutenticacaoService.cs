﻿using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using PUC.TCC.Application.Services.Autenticacao.Dtos;
using PUC.TCC.Domain.Models;
using PUC.TCC.Infra;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace PUC.TCC.Application.Services.Autenticacao
{
    public class AutenticacaoService : IAutenticacaoService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly SigningConfigurations _signingConfigurations;
        private readonly TokenConfigurations _tokenConfigurations;
        private readonly RoleManager<IdentityRole> _roleManager;

        public AutenticacaoService(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            SigningConfigurations signingConfigurations,
            TokenConfigurations tokenConfigurations,
            RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _signingConfigurations = signingConfigurations;
            _tokenConfigurations = tokenConfigurations;
            _roleManager = roleManager;
        }

        public void Cadastar(UsuarioCreateDto usuario)
        {
            if (_userManager.FindByEmailAsync(usuario.Email).Result == null)
            {
                var appUser = new ApplicationUser { Email = usuario.Email, UserName = usuario.Name };
                var user = _userManager.CreateAsync(appUser, usuario.Password).Result;

                if (user.Succeeded)
                {
                    var existRole = _roleManager.FindByNameAsync(Roles.ROLE_ADMIN).Result;
                    if (existRole == null)
                    {
                        var ux = _roleManager.CreateAsync(new IdentityRole(Roles.ROLE_ADMIN)).Result;
                    }

                    _userManager.AddToRoleAsync(appUser, Roles.ROLE_ADMIN);
                }
            }
        }

        public TokenDto Login(UsuarioDto usuario)
        {
            bool credenciaisValidas = false;
            ApplicationUser userIdentity = null;
            if (usuario != null && !String.IsNullOrWhiteSpace(usuario.Email))
            {
                // Verifica a existência do usuário nas tabelas do
                // ASP.NET Core Identity
                userIdentity = _userManager.FindByEmailAsync(usuario.Email).Result;
                if (userIdentity != null)
                {
                    // Efetua o login com base no Id do usuário e sua senha
                    var resultadoLogin = _signInManager
                        .CheckPasswordSignInAsync(userIdentity, usuario.Password, false).Result;

                    if (resultadoLogin.Succeeded)
                    {
                        // Verifica se o usuário em questão possui
                        // a role Acesso-APIAlturas
                        credenciaisValidas = _userManager.IsInRoleAsync(userIdentity, Roles.ROLE_ADMIN).Result;
                    }
                }
            }

            if (credenciaisValidas)
            {
                var identity = new ClaimsIdentity(
                    new GenericIdentity(usuario.Email, "Login"),
                    new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.UniqueName, usuario.Email)
                    }
                );

                DateTime dataCriacao = DateTime.Now;
                DateTime dataExpiracao = dataCriacao +
                    TimeSpan.FromSeconds(_tokenConfigurations.Seconds);

                var handler = new JwtSecurityTokenHandler();
                var securityToken = handler.CreateToken(new SecurityTokenDescriptor
                {
                    Issuer = _tokenConfigurations.Issuer,
                    Audience = _tokenConfigurations.Audience,
                    SigningCredentials = _signingConfigurations.SigningCredentials,
                    Subject = identity,
                    NotBefore = dataCriacao,
                    Expires = dataExpiracao
                });
                var token = handler.WriteToken(securityToken);

                return new TokenDto
                {
                    Authenticated = true,
                    Created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                    Expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                    AccessToken = token,
                    NameUser = userIdentity.UserName,
                    Message = "OK"
                };
            }
            else
            {
                return new TokenDto
                {
                    Authenticated = false,
                    Message = "Falha ao autenticar"
                };
            }
        }

        public async Task Logout()
        {
            await _signInManager.SignOutAsync();
        }
    }
}
