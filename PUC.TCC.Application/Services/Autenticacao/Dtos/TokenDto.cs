﻿namespace PUC.TCC.Application.Services.Autenticacao.Dtos
{
    public class TokenDto
    {
        public string Message { get; set; }
        public string AccessToken { get; set; }
        public string NameUser { get; set; }
        public string Expiration { get; set; }
        public string Created { get; set; }
        public bool Authenticated { get; set; }
    }
}
