﻿namespace PUC.TCC.Application.Services.Autenticacao.Dtos
{
    public class UsuarioDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class UsuarioCreateDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
    }
}
