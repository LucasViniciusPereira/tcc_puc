﻿using PUC.TCC.Application.Services.IntegracaoS2id.Dto;
using PUC.TCC.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace PUC.TCC.Application.Services.IntegracaoS2id
{
    public class IntegracaoS2idService : IIntegracaoS2idService
    {
        private readonly IIntegracaoS2idRepository _repository;
        public IntegracaoS2idService(IIntegracaoS2idRepository repository)
        {
            _repository = repository;
        }

        public Domain.Models.IntegracaoS2id Buscar(Expression<Func<Domain.Models.IntegracaoS2id, bool>> expression)
        {
            return _repository.Get().Where(expression).SingleOrDefault();
        }

        public IEnumerable<IntegracaoS2idDto> BuscarTodos()
        {
            return _repository.Get().Select(c => new IntegracaoS2idDto
            {
                Ativo = c.Ativo,
                Cobrade = c.Cobrade,
                DataDecreto = c.DataDecreto.ToShortDateString(),
                DataIntegracao = $"{c.DataIntegracao.ToShortDateString()} - {c.DataIntegracao.ToShortTimeString()}",
                Descricao = c.Descricao,
                Id = c.Id,
                Latitude = c.Latitude.ToString(),
                Longitude = c.Longitude.ToString(),
                Municipio = c.Municipio,
                Processo = c.Processo,
                Regiao = c.Regiao,
                Rito = c.Rito,
                SeEcp = c.SeEcp,
                Tipo = c.Tipo,
                Uf = c.Uf,
                Site = c.Site
            });
        }

        public void Salvar(Domain.Models.IntegracaoS2id model)
        {
            model.DataIntegracao = DateTime.Now;
            _repository.Add(model);
        }

        public void Dispose()
        {
            _repository.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
