﻿using System;

namespace PUC.TCC.Application.Services.IntegracaoS2id.Dto
{
    public class IntegracaoS2idDto
    {
        public int Id { get; set; }
        public int Cobrade { get; set; }
        public string Descricao { get; set; }
        public string Tipo { get; set; }
        public string Processo { get; set; }
        public string Rito { get; set; }
        public string SeEcp { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string DataDecreto { get; set; }
        public string Municipio { get; set; }
        public string Regiao { get; set; }
        public string Uf { get; set; }
        public bool Ativo { get; set; }
        public string Site { get; set; }
        public string DataIntegracao { get; set; }
    }
}
