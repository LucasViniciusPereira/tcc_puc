﻿using PUC.TCC.Application.Services.IntegracaoS2id.Dto;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Model = PUC.TCC.Domain.Models;

namespace PUC.TCC.Application.Services.IntegracaoS2id
{
    public interface IIntegracaoS2idService : IDisposable
    {
        Model.IntegracaoS2id Buscar(Expression<Func<Model.IntegracaoS2id, bool>> expression);
        IEnumerable<IntegracaoS2idDto> BuscarTodos();
        void Salvar(Model.IntegracaoS2id model);
    }
}
