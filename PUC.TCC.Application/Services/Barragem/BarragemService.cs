﻿using PUC.TCC.Application.Services.Barragem.Dtos;
using PUC.TCC.Application.Services.RiscoRompimento.Dtos;
using PUC.TCC.Domain.Enuns;
using PUC.TCC.Domain.Interfaces;
using PUC.TCC.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Model = PUC.TCC.Domain.Models;

namespace PUC.TCC.Application.Services.Barragem
{
    public class BarragemService : IBarragemService
    {
        private readonly IBarragemRepository _repo;

        public BarragemService(IBarragemRepository repository)
        {
            _repo = repository;
        }

        public IEnumerable<BarragemListagemDtoOutput> BuscarBarragem()
        {
            return _repo.Get()
                .Select(c => new BarragemListagemDtoOutput
                {
                    BarragemId = c.BarragemId,
                    Latitude = c.Latitude,
                    Longitude = c.Longitude,
                    Nome = c.Nome,
                    EStatusBarragem = c.EStatusBarragem
                });
        }

        public IEnumerable<BarragemListagemDtoOutput> PesquisarBarragem(string nome)
        {
            var nomeBarragem = nome.ToUpper();

            return _repo.Get()
                 .Where(c => c.Nome.ToUpper().Contains(nomeBarragem))
                 .Select(c => new BarragemListagemDtoOutput
                 {
                     BarragemId = c.BarragemId,
                     Latitude = c.Latitude,
                     Longitude = c.Longitude,
                     Nome = c.Nome,
                     EStatusBarragem = c.EStatusBarragem
                 });
        }

        public BarragemDtoOutput BuscarBarragem(int id)
        {
            var barragem = _repo.Get(id);

            if (barragem == null)
                return null;

            return new BarragemDtoOutput
            {
                BarragemId = barragem.BarragemId,
                Ativo = barragem.Ativo,
                CapacidadeAtual = barragem.CapacidadeAtual,
                CapacidadeTotal = barragem.CapacidadeTotal,
                Latitude = barragem.Latitude,
                Longitude = barragem.Longitude,
                Nome = barragem.Nome,
                Empresa = barragem.Empresa,
                EStatusBarragem = barragem.EStatusBarragem,
                ETipoBarragem = barragem.ETipoBarragem,
                Medidor = barragem.Medidor?.Select(c => new MedidorDtoOutput
                {
                    Nome = c.Nome,
                    BarragemId = c.BarragemId,
                    ETipoMedidor = c.ETipoMedidor,
                    MedidorId = c.MedidorId,
                    Codigo = c.Codigo
                }),
                BarragemNotificacao = barragem.BarragemNotificacao
                ?.Where(c => c.Notificacao.Ativo)
                ?.Select(c => new BarragemNotificacaoDto
                {
                    BarragemId = c.BarragemId,
                    MedidorId = c.MedidorId,
                    Medidor = c.Medidor.Nome,
                    BarragemNotificacaoId = c.BarragemNotificacaoId,
                    DataCricao = c.Notificacao.DataCricao,
                    Descricao = c.Notificacao.Descricao,
                    NotificacaoId = c.NotificacaoId,
                    ETipoCriticidade = c.Notificacao.ETipoCriticidade
                }).OrderByDescending(c => c.DataCricao),
                RiscoRompimento = barragem.RiscoRompimento?
                .Where(c => c.Ativo != false && c.Rompimento != false)
                .Select(c => new RiscoRompimentoDtoInput
                {
                    RiscoRompimentoId = c.RiscoRompimentoId,
                    DataOcorrencia = c.DataOcorrencia,
                    Mensagem = c.Mensagem,
                    Rompimento = c.Rompimento,
                    Ativo = c.Ativo
                }).LastOrDefault()
            };
        }

        public bool CriarBarragem(BarragemDtoInput barragem)
        {
            var model = new Model.Barragem
            {
                ETipoBarragem = barragem.ETipoBarragem,
                Ativo = true,
                CapacidadeAtual = barragem.CapacidadeAtual,
                CapacidadeTotal = barragem.CapacidadeTotal,
                Latitude = barragem.Latitude,
                Longitude = barragem.Longitude,
                Nome = barragem.Nome,
                Empresa = barragem.Empresa
            }.DefinirStatusBarragem();


            _repo.Add(model);
            return _repo.SaveChanges();
        }

        public bool VincularMedidor(MedidorDtoInput medidor)
        {
            var barragem = _repo.Get(medidor.BarragemId);

            if (barragem == null)
                throw new Exception($"Não foi possível localizar a barragem {medidor.BarragemId}");

            var model = new Medidor
            {
                BarragemId = medidor.BarragemId,
                ETipoMedidor = medidor.ETipoMedidor,
                Nome = medidor.Nome,
                Codigo = medidor.Codigo.ToUpper()
            };

            _repo.AddMedidor(model);
            return _repo.SaveChanges();
        }

        public bool DesativarBarragem(int id)
        {
            var barragem = _repo.Get(id);

            if (barragem == null)
                throw new Exception($"Não foi possível localizar a barragem {id}");

            barragem.DesativarBarragem();

            return _repo.SaveChanges();
        }

        public bool AlterarCapacidadeBarragem(int id, decimal novaCapacidade)
        {
            var barragem = _repo.Get(id);

            if (barragem == null)
                throw new Exception($"Não foi possível localizar a barragem {id}");

            barragem.CapacidadeAtual = novaCapacidade;
            return _repo.SaveChanges();
        }

        public void Dispose()
        {
            _repo.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
