﻿using PUC.TCC.Application.Services.Barragem.Dtos;
using System;
using System.Collections.Generic;

namespace PUC.TCC.Application.Services.Barragem
{
    public interface IBarragemService : IDisposable
    {
        BarragemDtoOutput BuscarBarragem(int id);
        IEnumerable<BarragemListagemDtoOutput> BuscarBarragem();
        IEnumerable<BarragemListagemDtoOutput> PesquisarBarragem(string nome);
        bool CriarBarragem(BarragemDtoInput barragem);
        bool VincularMedidor(MedidorDtoInput medidor);
        bool DesativarBarragem(int id);
        bool AlterarCapacidadeBarragem(int id, decimal novaCapacidade);
    }
}
