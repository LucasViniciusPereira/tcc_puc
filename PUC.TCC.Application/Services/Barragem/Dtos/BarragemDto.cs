﻿using PUC.TCC.Application.Services.RiscoRompimento.Dtos;
using PUC.TCC.Domain.Enuns;
using System.Collections.Generic;

namespace PUC.TCC.Application.Services.Barragem.Dtos
{
    public class BarragemDtoOutput
    {
        public int BarragemId { get; set; }
        public string Nome { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public bool Ativo { get; set; }
        public decimal CapacidadeTotal { get; set; }
        public decimal CapacidadeAtual { get; set; }
        public EStatusBarragem EStatusBarragem { get; set; }
        public string Empresa { get; set; }
        public ETipoBarragem ETipoBarragem { get; set; }
        public IEnumerable<MedidorDtoOutput> Medidor { get; set; }
        public IEnumerable<BarragemNotificacaoDto> BarragemNotificacao { get; set; }
        public RiscoRompimentoDtoInput RiscoRompimento { get; set; }
    }

    public class BarragemListagemDtoOutput
    {
        public int BarragemId { get; set; }
        public string Nome { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public EStatusBarragem EStatusBarragem { get; set; }
    }

    public class BarragemDtoInput
    {
        public string Nome { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public decimal CapacidadeTotal { get; set; }
        public decimal CapacidadeAtual { get; set; }
        public string Empresa { get; set; }
        public ETipoBarragem ETipoBarragem { get; set; }
    }
}
