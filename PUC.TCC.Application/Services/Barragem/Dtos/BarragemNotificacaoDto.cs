﻿using PUC.TCC.Domain.Enuns;
using System;

namespace PUC.TCC.Application.Services.Barragem.Dtos
{
    public class BarragemNotificacaoDto
    {
        public int BarragemNotificacaoId { get; set; }
        public int BarragemId { get; set; }
        public int MedidorId { get; set; }
        public string Medidor { get; set; }
        public int NotificacaoId { get; set; }
        public string Descricao { get; set; }
        public DateTime DataCricao { get; set; }
        public ETipoCriticidade ETipoCriticidade { get; set; }
    }
}
