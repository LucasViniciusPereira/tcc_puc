﻿using PUC.TCC.Domain.Enuns;

namespace PUC.TCC.Application.Services.Barragem.Dtos
{
    public class MedidorDtoOutput
    {
        public int MedidorId { get; set; }
        public string Nome { get; set; }
        public string Codigo { get; set; }
        public ETipoMedidor ETipoMedidor { get; set; }
        public int BarragemId { get; set; }
    }

    public class MedidorDtoInput
    {
        public string Nome { get; set; }
        public string Codigo { get; set; }
        public ETipoMedidor ETipoMedidor { get; set; }
        public int BarragemId { get; set; }
    }
}
