﻿using Moq;
using PUC.TCC.Application.Services.Notificacao;
using PUC.TCC.Domain.Interfaces;
using PUC.TCC.Domain.Models;
using System;
using Xunit;

namespace PUC.TCC.Test
{
    public class NotificacaoTest
    {
        private readonly INotificacaoService _service;
        private readonly Mock<IBarragemRepository> _repoBarragem;
        private readonly Mock<INotificacaoRepository> _repoNotificacao;

        public NotificacaoTest()
        {
            _repoBarragem = new Mock<IBarragemRepository>();
            _repoNotificacao = new Mock<INotificacaoRepository>();
            _service = new NotificacaoService(_repoNotificacao.Object, _repoBarragem.Object);
        }

        [Fact]
        public void Notificar_Barragem_Sucesso()
        {
            _repoBarragem.Setup(c => c.Get(It.IsAny<int>())).Returns(Factory.Barragem);
            _repoBarragem.Setup(c => c.GetMedidor(It.IsAny<int>())).Returns(Factory.Medidor);
            _repoNotificacao.Setup(c => c.SaveChanges()).Returns(true);

            var result = _service.NotificarBarragem(Factory.NotificacaoDtoInput);

            Assert.True(result);
        }

        [Fact]
        public void Notificar_Barragem_Excecao()
        {
            _repoBarragem.Setup(c => c.Get(It.IsAny<int>())).Returns<Barragem>(null);
            _repoBarragem.Setup(c => c.GetMedidor(It.IsAny<int>())).Returns<Medidor>(null);

            Assert.Throws<Exception>(() => _service.NotificarBarragem(Factory.NotificacaoDtoInput));
        }
    }
}
