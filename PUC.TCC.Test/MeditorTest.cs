﻿using Moq;
using PUC.TCC.Application.Services.Barragem;
using PUC.TCC.Domain.Interfaces;
using PUC.TCC.Domain.Models;
using Xunit;

namespace PUC.TCC.Test
{
    public class MeditorTest
    {
        private readonly Mock<IBarragemService> _service;
        private readonly Mock<IBarragemRepository> _repo;

        public MeditorTest()
        {
            _service = new Mock<IBarragemService>();
            _repo = new Mock<IBarragemRepository>();
        }

        [Fact]
        public void Medidor_Vincular_Sucesso()
        {
            _service.Setup(c => c.VincularMedidor(Factory.MedidorDtoInput)).Returns(true);
            _repo.Setup(c => c.Get(It.IsAny<int>())).Returns(Factory.Barragem);

            var result = _service.Object.VincularMedidor(Factory.MedidorDtoInput);

            Assert.True(result);
        }

        [Fact]
        public void Medidor_Vincular_SemBarragem()
        {
            _service.Setup(c => c.VincularMedidor(Factory.MedidorDtoInput)).Returns(false);
            _repo.Setup(c => c.Get(It.IsAny<int>())).Returns<Barragem>(null);

            var result = _service.Object.VincularMedidor(Factory.MedidorDtoInput);

            Assert.False(result);
        }
    }
}
