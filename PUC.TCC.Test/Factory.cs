﻿using PUC.TCC.Application.Services.Barragem.Dtos;
using PUC.TCC.Application.Services.Notificacao.Dtos;
using PUC.TCC.Domain.Enuns;
using PUC.TCC.Domain.Models;
using System;
using System.Collections.Generic;

namespace PUC.TCC.Test
{
    public static class Factory
    {
        public static List<BarragemListagemDtoOutput> BuscarBarragens()
        {
            return new List<BarragemListagemDtoOutput> {
                new BarragemListagemDtoOutput {
                    BarragemId = 1,
                    EStatusBarragem = EStatusBarragem.Estavel,
                     Latitude = -11.00,
                     Longitude = -10.99,
                     Nome = "Barragem Ouro Branco"
                },
                 new BarragemListagemDtoOutput {
                    BarragemId = 2,
                    EStatusBarragem = EStatusBarragem.Desativada,
                     Latitude = -11.00,
                     Longitude = -10.99,
                     Nome = "Barragem Brumadinho"
                }
            };
        }

        public static BarragemDtoOutput BarragemDtoOutput = new BarragemDtoOutput
        {
            Ativo = true,
            BarragemId = 1,
            CapacidadeAtual = 100,
            CapacidadeTotal = 200,
            Empresa = "Vale",
            EStatusBarragem = EStatusBarragem.Estavel,
            Nome = "Barragem de teste"
        };

        public static Barragem Barragem = new Barragem { BarragemId = 1, Ativo = true, Empresa = "Vale", BarragemNotificacao = BarragemNotificacaos };

        public static Medidor Medidor = new Medidor { BarragemId = 1, Codigo = "01234AB", Nome = "Medidor Teste" };

        public static MedidorDtoInput MedidorDtoInput = new MedidorDtoInput { BarragemId = 1, Codigo = "01ABC2", Nome = "Sensor", ETipoMedidor = ETipoMedidor.Piezometros };

        public static NotificacaoDtoInput NotificacaoDtoInput = new NotificacaoDtoInput { ETipoCriticidade = ETipoCriticidade.Baixo, MedidorId = 1, Mensagem = "Mensagem de teste" };

        public static BarragemNotificacao BarragemNotificacao = new BarragemNotificacao { BarragemId = 1, Barragem = Factory.Barragem, Medidor = Factory.Medidor, MedidorId = 1 };

        public static List<BarragemNotificacao> BarragemNotificacaos = new List<BarragemNotificacao> { new BarragemNotificacao { BarragemId = 1, Barragem = Factory.Barragem, Medidor = Factory.Medidor, MedidorId = 1 } };

        public static RiscoRompimento RiscoRompimento = new RiscoRompimento { RiscoRompimentoId = 1, BarragemId = 1, Mensagem = "Risco de rompimento", Barragem = Factory.Barragem };

        public static List<RiscoRompimento> RiscoRompimentos = new List<RiscoRompimento> { new RiscoRompimento { RiscoRompimentoId = 1, BarragemId = 1, Mensagem = "Risco de rompimento", Barragem = Factory.Barragem } };
    }
}
