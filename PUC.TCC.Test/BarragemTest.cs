﻿using Moq;
using PUC.TCC.Application.Services.Barragem;
using PUC.TCC.Application.Services.Barragem.Dtos;
using System.Linq;
using Xunit;

namespace PUC.TCC.Test
{
    public class BarragemTest
    {
        private readonly Mock<IBarragemService> _service;

        public BarragemTest()
        {
            _service = new Mock<IBarragemService>();
        }

        [Fact]
        public void Barragem_ListarTodas_Sucesso()
        {
            _service.Setup(c => c.BuscarBarragem()).Returns(Factory.BuscarBarragens());

            var result = _service.Object.BuscarBarragem();

            Assert.True(result.Any());
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void Barragem_BuscarPorId_Sucesso(int value)
        {
            _service.Setup(c => c.BuscarBarragem(value)).Returns(Factory.BarragemDtoOutput);

            var result = _service.Object.BuscarBarragem(value);

            Assert.IsType<BarragemDtoOutput>(result);
            Assert.NotNull(result);
        }

        [Theory]
        [InlineData("Barragem Brumadinho")]
        public void Barragem_Pesquisar_Sucesso(string value)
        {
            _service.Setup(c => c.PesquisarBarragem(It.IsAny<string>())).Returns(Factory.BuscarBarragens().Where(c => c.Nome.Contains(value)));

            var result = _service.Object.PesquisarBarragem(value);

            Assert.True(result.Any());
            Assert.Contains(result, c => c.Nome == value);
        }
    }
}
