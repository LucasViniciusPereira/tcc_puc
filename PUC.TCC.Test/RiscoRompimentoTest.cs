﻿using Moq;
using PUC.TCC.Application.Services.RiscoRompimento;
using PUC.TCC.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace PUC.TCC.Test
{
    public class RiscoRompimentoTest
    {
        private readonly IRiscoRompimentoService _service;
        private readonly Mock<IBarragemRepository> _repoBarragem;
        private readonly Mock<IRiscoRompimentoRepository> _repoRisco;

        public RiscoRompimentoTest()
        {
            _repoBarragem = new Mock<IBarragemRepository>();
            _repoRisco = new Mock<IRiscoRompimentoRepository>();
            _service = new RiscoRompimentoService(_repoBarragem.Object, _repoRisco.Object);
        }

        [Theory]
        [InlineData(1)]
        public void RiscoRompimento_Emitir_Sucesso(int value)
        {
            _repoBarragem.Setup(c => c.Get(It.IsAny<int>())).Returns(Factory.Barragem);

            Assert.Throws<NullReferenceException>(() => _service.EmitirRiscoDeRompimento(value));
        }

        [Theory]
        [InlineData(1)]
        public void RiscoRompimento_Cancelar_Sucesso(int value)
        {
            _repoRisco.Setup(c => c.Get()).Returns(Factory.RiscoRompimentos.AsQueryable());

            Assert.Throws<NullReferenceException>(() => _service.CancelarRiscoDeRompimento(value));
        }

        [Theory]
        [InlineData(1)]
        public void RiscoRompimento_Confirmar_Sucesso(int value)
        {
            _repoRisco.Setup(c => c.Get()).Returns(Factory.RiscoRompimentos.AsQueryable());

            var result = _service.ConfirmarRompimento(value);

            Assert.True(result > 0);
            Assert.Equal(1, result);
        }
    }
}
