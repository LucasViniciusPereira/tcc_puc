﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PUC.TCC.Infra.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Barragem",
                columns: table => new
                {
                    BarragemId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EStatusBarragem = table.Column<short>(nullable: false),
                    Nome = table.Column<string>(type: "varchar(150)", nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    CapacidadeTotal = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    CapacidadeAtual = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    Empresa = table.Column<string>(type: "varchar(150)", nullable: false),
                    ETipoBarragem = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Barragem", x => x.BarragemId);
                });

            migrationBuilder.CreateTable(
                name: "IntegracaoS2id",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Cobrade = table.Column<int>(nullable: false),
                    Descricao = table.Column<string>(type: "varchar(500)", nullable: false),
                    Tipo = table.Column<string>(type: "varchar(150)", nullable: false),
                    Processo = table.Column<string>(type: "varchar(300)", nullable: false),
                    Rito = table.Column<string>(type: "varchar(50)", nullable: false),
                    SeEcp = table.Column<string>(type: "varchar(50)", nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    DataDecreto = table.Column<DateTime>(nullable: false),
                    Municipio = table.Column<string>(type: "varchar(50)", nullable: false),
                    Regiao = table.Column<string>(type: "varchar(50)", nullable: false),
                    Uf = table.Column<string>(type: "varchar(2)", nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    DataIntegracao = table.Column<DateTime>(nullable: false),
                    Site = table.Column<string>(type: "varchar(150)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IntegracaoS2id", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Notificacao",
                columns: table => new
                {
                    NotificacaoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Descricao = table.Column<string>(type: "varchar(200)", nullable: false),
                    DataCricao = table.Column<DateTime>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    ETipoCriticidade = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notificacao", x => x.NotificacaoId);
                });

            migrationBuilder.CreateTable(
                name: "Medidor",
                columns: table => new
                {
                    MedidorId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nome = table.Column<string>(type: "varchar(150)", nullable: false),
                    Codigo = table.Column<string>(type: "varchar(20)", nullable: false),
                    ETipoMedidor = table.Column<short>(nullable: false),
                    BarragemId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Medidor", x => x.MedidorId);
                    table.ForeignKey(
                        name: "FK_Medidor_Barragem_BarragemId",
                        column: x => x.BarragemId,
                        principalTable: "Barragem",
                        principalColumn: "BarragemId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RiscoRompimento",
                columns: table => new
                {
                    RiscoRompimentoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BarragemId = table.Column<int>(nullable: false),
                    Rompimento = table.Column<bool>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    DataOcorrencia = table.Column<DateTime>(nullable: true),
                    Mensagem = table.Column<string>(type: "varchar(200)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RiscoRompimento", x => x.RiscoRompimentoId);
                    table.ForeignKey(
                        name: "FK_RiscoRompimento_Barragem_BarragemId",
                        column: x => x.BarragemId,
                        principalTable: "Barragem",
                        principalColumn: "BarragemId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BarragemNotificacao",
                columns: table => new
                {
                    BarragemNotificacaoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BarragemId = table.Column<int>(nullable: false),
                    MedidorId = table.Column<int>(nullable: false),
                    NotificacaoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BarragemNotificacao", x => x.BarragemNotificacaoId);
                    table.ForeignKey(
                        name: "FK_BarragemNotificacao_Barragem_BarragemId",
                        column: x => x.BarragemId,
                        principalTable: "Barragem",
                        principalColumn: "BarragemId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BarragemNotificacao_Medidor_MedidorId",
                        column: x => x.MedidorId,
                        principalTable: "Medidor",
                        principalColumn: "MedidorId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BarragemNotificacao_Notificacao_NotificacaoId",
                        column: x => x.NotificacaoId,
                        principalTable: "Notificacao",
                        principalColumn: "NotificacaoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BarragemNotificacao_BarragemId",
                table: "BarragemNotificacao",
                column: "BarragemId");

            migrationBuilder.CreateIndex(
                name: "IX_BarragemNotificacao_MedidorId",
                table: "BarragemNotificacao",
                column: "MedidorId");

            migrationBuilder.CreateIndex(
                name: "IX_BarragemNotificacao_NotificacaoId",
                table: "BarragemNotificacao",
                column: "NotificacaoId");

            migrationBuilder.CreateIndex(
                name: "IX_Medidor_BarragemId",
                table: "Medidor",
                column: "BarragemId");

            migrationBuilder.CreateIndex(
                name: "IX_RiscoRompimento_BarragemId",
                table: "RiscoRompimento",
                column: "BarragemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BarragemNotificacao");

            migrationBuilder.DropTable(
                name: "IntegracaoS2id");

            migrationBuilder.DropTable(
                name: "RiscoRompimento");

            migrationBuilder.DropTable(
                name: "Medidor");

            migrationBuilder.DropTable(
                name: "Notificacao");

            migrationBuilder.DropTable(
                name: "Barragem");
        }
    }
}
