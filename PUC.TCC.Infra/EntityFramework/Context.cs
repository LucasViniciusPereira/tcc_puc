﻿using Microsoft.EntityFrameworkCore;
using PUC.TCC.Domain.Models;
using PUC.TCC.Infra.EntityFramework.Mapping;

namespace PUC.TCC.Infra.EntityFramework
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options) { }

        public DbSet<Barragem> Barragem { get; set; }
        public DbSet<BarragemNotificacao> BarragemNotificacao { get; set; }
        public DbSet<Medidor> Medidor { get; set; }
        public DbSet<Notificacao> Notificacao { get; set; }
        public DbSet<RiscoRompimento> RiscoRompimento { get; set; }
        public DbSet<IntegracaoS2id> IntegracaoS2id { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new BarragemMap());
            modelBuilder.ApplyConfiguration(new BarragemNotificacaoMap());
            modelBuilder.ApplyConfiguration(new MedidorMap());
            modelBuilder.ApplyConfiguration(new NotificacaoMap());
            modelBuilder.ApplyConfiguration(new RiscoRompimentoMap());
            modelBuilder.ApplyConfiguration(new IntegracaoS2idMap());
        }
    }
}
