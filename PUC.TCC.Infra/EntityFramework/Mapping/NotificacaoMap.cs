﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PUC.TCC.Domain.Models;
using System;

namespace PUC.TCC.Infra.EntityFramework.Mapping
{
    public class NotificacaoMap : IEntityTypeConfiguration<Notificacao>
    {
        public void Configure(EntityTypeBuilder<Notificacao> builder)
        {
            builder.ToTable("Notificacao");

            builder.Property(c => c.Descricao)
                 .IsRequired()
                 .HasColumnType("varchar(200)");
        }
    }
}
