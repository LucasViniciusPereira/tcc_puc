﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PUC.TCC.Domain.Models;

namespace PUC.TCC.Infra.EntityFramework.Mapping
{
    public class RiscoRompimentoMap : IEntityTypeConfiguration<RiscoRompimento>
    {
        public void Configure(EntityTypeBuilder<RiscoRompimento> builder)
        {
            builder.ToTable("RiscoRompimento");

            builder.HasKey(c => c.RiscoRompimentoId);

            builder.Property(c => c.Mensagem)
                .HasColumnType("varchar(200)");

            builder.HasOne(c => c.Barragem);
        }
    }
}
