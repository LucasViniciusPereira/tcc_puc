﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PUC.TCC.Domain.Models;

namespace PUC.TCC.Infra.EntityFramework.Mapping
{
    public class BarragemNotificacaoMap : IEntityTypeConfiguration<BarragemNotificacao>
    {
        public void Configure(EntityTypeBuilder<BarragemNotificacao> builder)
        {
            builder.ToTable("BarragemNotificacao");

            builder.HasOne(c => c.Barragem);
            builder.HasOne(c => c.Medidor).WithMany().OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(c => c.Notificacao);
        }
    }
}
