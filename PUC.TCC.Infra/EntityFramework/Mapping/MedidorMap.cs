﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PUC.TCC.Domain.Models;
using System;

namespace PUC.TCC.Infra.EntityFramework.Mapping
{
    public class MedidorMap : IEntityTypeConfiguration<Medidor>
    {
        public void Configure(EntityTypeBuilder<Medidor> builder)
        {
            builder.ToTable("Medidor");

            builder.Property(c => c.Nome)
               .IsRequired()
               .HasColumnType("varchar(150)");

            builder.Property(c => c.Codigo)
                .IsRequired()
                .HasColumnType("varchar(20)");

            builder.HasOne(c => c.Barragem);
        }
    }
}
