﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PUC.TCC.Domain.Models;

namespace PUC.TCC.Infra.EntityFramework.Mapping
{
    internal class IntegracaoS2idMap : IEntityTypeConfiguration<IntegracaoS2id>
    {
        public void Configure(EntityTypeBuilder<IntegracaoS2id> builder)
        {
            builder.ToTable("IntegracaoS2id");

            builder.Property(c => c.Id);

            builder.Property(c => c.Descricao)
                 .IsRequired()
                 .HasColumnType("varchar(500)");

            builder.Property(c => c.Municipio)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Processo)
                .IsRequired()
                .HasColumnType("varchar(300)");

            builder.Property(c => c.Regiao)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Rito)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.SeEcp)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Tipo)
                .IsRequired()
                .HasColumnType("varchar(150)");
            builder.Property(c => c.Site)
               .IsRequired()
               .HasColumnType("varchar(150)");

            builder.Property(c => c.Uf)
                .IsRequired()
                .HasColumnType("varchar(2)");
        }
    }
}