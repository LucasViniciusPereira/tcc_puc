﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PUC.TCC.Domain.Models;

namespace PUC.TCC.Infra.EntityFramework.Mapping
{
    public class BarragemMap : IEntityTypeConfiguration<Barragem>
    {
        public void Configure(EntityTypeBuilder<Barragem> builder)
        {
            builder.ToTable("Barragem");

            builder.Property(c => c.Nome)
                .IsRequired()
                .HasColumnType("varchar(150)");

            builder.Property(c => c.Empresa)
              .IsRequired()
              .HasColumnType("varchar(150)");

            builder.Property(c => c.Ativo)
                .IsRequired();

            builder.Property(c => c.CapacidadeTotal)
                .HasColumnType("decimal(10,2)");

            builder.Property(c => c.CapacidadeAtual)
                .IsRequired()
                .HasColumnType("decimal(10,2)");

            builder.Property(c => c.Latitude)
                .IsRequired();

            builder.Property(c => c.Longitude)
                .IsRequired();

            builder.HasMany(c => c.BarragemNotificacao)
                .WithOne(e => e.Barragem)
                .HasForeignKey(c => c.BarragemId);

            builder.HasMany(c => c.Medidor)
              .WithOne(e => e.Barragem)
              .HasForeignKey(c => c.BarragemId);

            builder.HasMany(c => c.RiscoRompimento)
                .WithOne(e => e.Barragem)
                .HasForeignKey(c => c.BarragemId);
        }
    }
}
