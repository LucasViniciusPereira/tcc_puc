﻿using PUC.TCC.Domain.Interfaces;
using PUC.TCC.Domain.Models;
using PUC.TCC.Infra.EntityFramework;

namespace PUC.TCC.Infra.Repository
{
    public class NotificacaoRepository : RepositoryBase<Notificacao>, INotificacaoRepository
    {
        public NotificacaoRepository(Context context) : base(context) { }

        public void AddBarragemNotificacao(BarragemNotificacao notificacao)
        {
            _context.Add(notificacao);
        }
    }
}
