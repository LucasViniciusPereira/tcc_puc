﻿using PUC.TCC.Domain.Interfaces;
using PUC.TCC.Domain.Models;
using PUC.TCC.Infra.EntityFramework;

namespace PUC.TCC.Infra.Repository
{
    public class RiscoRompimentoRepository : RepositoryBase<RiscoRompimento>, IRiscoRompimentoRepository
    {
        public RiscoRompimentoRepository(Context context) : base(context) { }
    }
}
