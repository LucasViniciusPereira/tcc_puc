﻿using PUC.TCC.Domain.Interfaces;
using PUC.TCC.Domain.Models;
using PUC.TCC.Infra.EntityFramework;

namespace PUC.TCC.Infra.Repository
{
    public class BarragemRepository : RepositoryBase<Barragem>, IBarragemRepository
    {
        public BarragemRepository(Context context) : base(context) { }

        public Medidor AddMedidor(Medidor model)
        {
            var result = _context.Add(model);
            return result.Entity;
        }

        public Barragem Get(int id)
        {
            return _context.Find<Barragem>(id);
        }

        public Medidor GetMedidor(int id)
        {
            return _context.Find<Medidor>(id);
        }
    }
}
