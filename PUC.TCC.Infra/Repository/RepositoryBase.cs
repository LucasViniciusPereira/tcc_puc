﻿using PUC.TCC.Domain.Interfaces;
using PUC.TCC.Infra.EntityFramework;
using System;
using System.Linq;

namespace PUC.TCC.Infra.Repository
{
    public abstract class RepositoryBase<T> : IRepository<T> where T : class
    {
        protected readonly Context _context;
        public RepositoryBase(Context context) => _context = context;

        public T Add(T entity)
        {
            var result = _context.Add(entity);
            return result.Entity;
        }

        public T Get(T entity)
        {
            var result = _context.Find<T>(entity);
            return result;
        }

        public IQueryable<T> Get()
        {
            return _context.Set<T>().AsQueryable();
        }

        public void Remove(T entity)
        {
            _context.Remove<T>(entity);
        }

        public T Update(T entity)
        {
            var result = _context.Update(entity);
            return result.Entity;
        }

        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() > 0;
        }
    }
}
