﻿using PUC.TCC.Domain.Interfaces;
using PUC.TCC.Domain.Models;
using PUC.TCC.Infra.EntityFramework;

namespace PUC.TCC.Infra.Repository
{
    public class IntegracaoS2idRepository : RepositoryBase<IntegracaoS2id>, IIntegracaoS2idRepository
    {
        public IntegracaoS2idRepository(Context context) : base(context) { }
    }
}
