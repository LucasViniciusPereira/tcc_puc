SET IDENTITY_INSERT [dbo].[Barragem] ON
INSERT INTO [dbo].[Barragem] ([BarragemId], [EStatusBarragem], [Nome], [Latitude], [Longitude], [Ativo], [CapacidadeTotal], [CapacidadeAtual], [Empresa], [ETipoBarragem]) VALUES (1, 1, N'Barragem Brumadinho', -20.1333233, -44.1894687, 1, CAST(1000.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), N'Vale', 1)
INSERT INTO [dbo].[Barragem] ([BarragemId], [EStatusBarragem], [Nome], [Latitude], [Longitude], [Ativo], [CapacidadeTotal], [CapacidadeAtual], [Empresa], [ETipoBarragem]) VALUES (2, 1, N'São Domingos - Barragem da Usina São Domingos', -21.100757, -49.0132659, 1, CAST(1000.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), N'Vale', 1)
INSERT INTO [dbo].[Barragem] ([BarragemId], [EStatusBarragem], [Nome], [Latitude], [Longitude], [Ativo], [CapacidadeTotal], [CapacidadeAtual], [Empresa], [ETipoBarragem]) VALUES (3, 1, N'Barragem dos Mottas', -22.8927772, -45.2320881, 1, CAST(1000.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), N'Vale', 1)
INSERT INTO [dbo].[Barragem] ([BarragemId], [EStatusBarragem], [Nome], [Latitude], [Longitude], [Ativo], [CapacidadeTotal], [CapacidadeAtual], [Empresa], [ETipoBarragem]) VALUES (4, 2, N'Barragem Açude Bananeiras', -6.6960919, -35.6751471, 1, CAST(1000.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), N'Vale', 1)
INSERT INTO [dbo].[Barragem] ([BarragemId], [EStatusBarragem], [Nome], [Latitude], [Longitude], [Ativo], [CapacidadeTotal], [CapacidadeAtual], [Empresa], [ETipoBarragem]) VALUES (5, 4, N'Barragem Lagoa de Disposição de Rejeito', -0.6470501,-57.0002933, 1, CAST(1000.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), N'Vale', 1)
INSERT INTO [dbo].[Barragem] ([BarragemId], [EStatusBarragem], [Nome], [Latitude], [Longitude], [Ativo], [CapacidadeTotal], [CapacidadeAtual], [Empresa], [ETipoBarragem]) VALUES (6,3, N'Barragem Garça Branca - PCH Garça Branca', -26.6036781,-57.9191261, 1, CAST(1000.00 AS Decimal(10, 2)), CAST(0.00 AS Decimal(10, 2)), N'Hydroelectric power plant', 1)
SET IDENTITY_INSERT [dbo].[Barragem] OFF


SET IDENTITY_INSERT [dbo].[Medidor] ON
INSERT INTO [dbo].[Medidor] ([MedidorId], [Nome], [Codigo], [ETipoMedidor], [BarragemId]) VALUES (1, N'Medidor 1 - Pressão da água', N'A68D52', 1, 1)
INSERT INTO [dbo].[Medidor] ([MedidorId], [Nome], [Codigo], [ETipoMedidor], [BarragemId]) VALUES (2, N'Medidor 2 - Pressão da água', N'R66AF2', 1, 2)
SET IDENTITY_INSERT [dbo].[Medidor] OFF

