﻿using PUC.TCC.Domain.Models;

namespace PUC.TCC.Domain.Interfaces
{
    public interface IBarragemRepository : IRepository<Barragem>
    {
        Barragem Get(int id);
        Medidor GetMedidor(int id);
        Medidor AddMedidor(Medidor model);
    }
}
