﻿using PUC.TCC.Domain.Models;

namespace PUC.TCC.Domain.Interfaces
{
    public interface IRiscoRompimentoRepository : IRepository<RiscoRompimento>
    {
    }
}
