﻿using PUC.TCC.Domain.Models;

namespace PUC.TCC.Domain.Interfaces
{
    public interface IIntegracaoS2idRepository : IRepository<IntegracaoS2id>
    {
    }
}
