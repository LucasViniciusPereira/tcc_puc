﻿using System;
using System.Linq;

namespace PUC.TCC.Domain.Interfaces
{
    public interface IRepository<T> : IDisposable where T : class
    {
        T Add(T entity);
        T Update(T entity);
        void Remove(T entity);

        T Get(T entity);
        IQueryable<T> Get();

        bool SaveChanges();
    }
}
