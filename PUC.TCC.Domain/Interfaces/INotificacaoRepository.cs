﻿using PUC.TCC.Domain.Models;

namespace PUC.TCC.Domain.Interfaces
{
    public interface INotificacaoRepository : IRepository<Notificacao>
    {
        void AddBarragemNotificacao(BarragemNotificacao notificacao);
    }
}
