﻿using System;
using System.Linq;

namespace PUC.TCC.Domain.Models
{
    public class RiscoRompimento
    {
        public int RiscoRompimentoId { get; set; }
        public int BarragemId { get; set; }
        public virtual Barragem Barragem { get; set; }
        public bool Rompimento { get; private set; } = false;
        public bool Ativo { get; private set; } = true;
        public DateTime? DataOcorrencia { get; private set; }
        public string Mensagem { get; set; }

        public RiscoRompimento()
        {

        }

        public RiscoRompimento(int barragemId, string mensagem)
        {
            BarragemId = barragemId;
            Mensagem = mensagem;
        }

        public void CancelarRiscoRompimento()
        {
            Ativo = false;
            Rompimento = false;
            Mensagem = "Ocorreu tudo bem, o risco de rompimento foi cancelado.";
            foreach (var item in Barragem.BarragemNotificacao)
                item.Notificacao.Ativo = false;

            Barragem.DefinirStatusBarragem();
        }

        public void ConfirmarRompimento()
        {
            Ativo = true;
            Rompimento = true;
            DataOcorrencia = DateTime.Now;
            Mensagem = "Infelizmente não conseguimos evitar o rompimento.";
            Barragem.DefinirStatusBarragem();
        }
    }
}
