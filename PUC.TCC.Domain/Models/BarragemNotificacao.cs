﻿namespace PUC.TCC.Domain.Models
{
    public class BarragemNotificacao
    {
        public int BarragemNotificacaoId { get; set; }
        public int BarragemId { get; set; }
        public virtual Barragem Barragem { get; set; }
        public int MedidorId { get; set; }
        public virtual Medidor Medidor { get; set; }
        public int NotificacaoId { get; set; }
        public virtual Notificacao Notificacao { get; set; }
    }
}
