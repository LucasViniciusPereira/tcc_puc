﻿using PUC.TCC.Domain.Enuns;
using System.Collections.Generic;
using System.Linq;

namespace PUC.TCC.Domain.Models
{
    public class Barragem
    {
        public int BarragemId { get; set; }
        public EStatusBarragem EStatusBarragem { get; private set; }
        public string Nome { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public bool Ativo { get; set; }
        public decimal CapacidadeTotal { get; set; }
        public decimal CapacidadeAtual { get; set; }
        public string Empresa { get; set; }
        public ETipoBarragem ETipoBarragem { get; set; }

        public virtual ICollection<Medidor> Medidor { get; set; }
        public virtual ICollection<BarragemNotificacao> BarragemNotificacao { get; set; }
        public virtual ICollection<RiscoRompimento> RiscoRompimento { get; set; }

        public Barragem DefinirStatusBarragem()
        {
            if (BarragemNotificacao == null || BarragemNotificacao.All(c => c.Notificacao.Ativo == false))
            {
                EStatusBarragem = EStatusBarragem.Estavel;
                return this;
            }

            if (RiscoRompimento.Any(c => c.Rompimento && c.Ativo))
            {
                EStatusBarragem = EStatusBarragem.Rompimento;
                return this;
            }

            var temRiscoBaixo = BarragemNotificacao.All(c => c.Notificacao.Ativo && c.Notificacao.ETipoCriticidade == ETipoCriticidade.Baixo) ||
               BarragemNotificacao.Any() == false;
            var temRiscoMedio = BarragemNotificacao.All(c => c.Notificacao.Ativo && c.Notificacao.ETipoCriticidade == ETipoCriticidade.Medio) ||
                BarragemNotificacao.Any(c => c.Notificacao.ETipoCriticidade == ETipoCriticidade.Alto) == false;
            var temRiscoAlto = BarragemNotificacao.All(c => c.Notificacao.Ativo && c.Notificacao.ETipoCriticidade == ETipoCriticidade.Alto) ||
                BarragemNotificacao.Any(c => c.Notificacao.ETipoCriticidade == ETipoCriticidade.Alto);

            if (temRiscoBaixo)
                EStatusBarragem = EStatusBarragem.Estavel;
            else if (temRiscoMedio)
                EStatusBarragem = EStatusBarragem.Alerta;
            else if (temRiscoAlto)
                EStatusBarragem = EStatusBarragem.Critico;
            else
                EStatusBarragem = EStatusBarragem.Indefinido;

            return this;
        }
        public Barragem DesativarBarragem()
        {
            Ativo = false;
            EStatusBarragem = EStatusBarragem.Desativada;

            return this;
        }
    }
}
