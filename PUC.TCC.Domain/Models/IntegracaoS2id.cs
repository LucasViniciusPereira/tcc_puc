﻿using System;

namespace PUC.TCC.Domain.Models
{
    public class IntegracaoS2id
    {
        public int Id { get; set; }
        public int Cobrade { get; set; }
        public string Descricao { get; set; }
        public string Tipo { get; set; }
        public string Processo { get; set; }
        public string Rito { get; set; }
        public string SeEcp { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DateTime DataDecreto { get; set; }
        public string Municipio { get; set; }
        public string Regiao { get; set; }
        public string Uf { get; set; }
        public bool Ativo { get; set; }
        public DateTime DataIntegracao { get; set; }
        public string Site { get; set; }
    }
}
