﻿using PUC.TCC.Domain.Enuns;
using System;

namespace PUC.TCC.Domain.Models
{
    public class Notificacao
    {
        public int NotificacaoId { get; set; }
        public string Descricao { get; set; }
        public DateTime DataCricao { get; set; }
        public bool Ativo { get; set; }
        public ETipoCriticidade ETipoCriticidade { get; set; }
    }
}
