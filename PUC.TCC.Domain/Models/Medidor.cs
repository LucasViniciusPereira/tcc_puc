﻿using PUC.TCC.Domain.Enuns;

namespace PUC.TCC.Domain.Models
{
    public class Medidor
    {
        public int MedidorId { get; set; }
        public string Nome { get; set; }
        public string Codigo { get; set; }
        public ETipoMedidor ETipoMedidor { get; set; }
        public int BarragemId { get; set; }
        public virtual Barragem Barragem { get; set; }
    }
}
