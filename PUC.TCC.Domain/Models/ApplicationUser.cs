﻿using Microsoft.AspNetCore.Identity;

namespace PUC.TCC.Domain.Models
{
    public class ApplicationUser : IdentityUser
    {

    }

    public static class Roles
    {
        public const string ROLE_ADMIN = "Administrador";
    }

    public class TokenConfigurations
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public int Seconds { get; set; }
    }
}
