﻿namespace PUC.TCC.Domain.Enuns
{
    public enum ETipoCriticidade : short
    {
        Indefinido = 0,
        Baixo = 1,
        Medio = 2,
        Alto = 3
    }
}
