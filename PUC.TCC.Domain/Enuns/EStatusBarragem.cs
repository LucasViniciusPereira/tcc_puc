﻿namespace PUC.TCC.Domain.Enuns
{
    public enum EStatusBarragem : short
    {
        Indefinido = 0,
        Estavel = 1,
        Alerta = 2,
        Critico = 3,
        Desativada = 4,
        Rompimento = 5,
    }
}
