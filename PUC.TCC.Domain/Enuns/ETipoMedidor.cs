﻿namespace PUC.TCC.Domain.Enuns
{
    public enum ETipoMedidor : short
    {
        Indefinido = 0,
        Piezometros = 1
    }
}
