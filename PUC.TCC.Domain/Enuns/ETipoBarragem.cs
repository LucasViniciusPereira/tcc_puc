﻿namespace PUC.TCC.Domain.Enuns
{
    public enum ETipoBarragem : short
    {
        Indefinido = 0,
        Montante = 1,
        Jusante = 2,
        LinhaCentro = 3,
        Desconhecido = 4
    }
}
